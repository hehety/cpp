#include <iostream>

using namespace std;


void ref_test1(int & a, int & b)
{
	int temp = a;
	a = b;
	b = temp;
}

// 当返回一个引用时，要注意被引用的对象不能超出作用域。
// 所以返回一个对局部变量的引用是不合法的，但是，可以返回一个对静态变量的引用。
int& ref_test2()
{
	int q;
	//! return q; // 在编译时发生错误
	static int x = 1;
	cout << &x << endl;
	return x;     // 安全，x 在函数作用域外依然是有效的
}



void cpp_ptr()
{
	int a = 123;
	int& b = a; // 引用

	cout << "b是a的int类型引用" << b << endl;


	int c = 1, d = 2;
	cout << "c=" << c << ", d=" << d << endl;
	ref_test1(c, d);
	cout << "c=" << c << ", d=" << d << endl;

	cout << &ref_test2() << endl;

}