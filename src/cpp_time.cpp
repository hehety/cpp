#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <ctime>

using namespace std;

void cpp_time()
{
	// 获取 自 1970 年 1 月 1 日以来经过的秒数
	time_t seconds = time(NULL);
	cout << "seconds:" << seconds << endl;

	// 获取当地时间的字符串指针，字符串形式 day month year hours:minutes:seconds year\n\0。
	time_t curtime;
	time(&curtime);
	char* c = ctime(&curtime);
	cout << "时间：" << c << endl;

	// 该函数返回一个指向表示本地时间的 tm 结构的指针
	tm *t;
	t = localtime(&curtime);
	cout << (*t).tm_year + 1900 << "-" << (*t).tm_mon+1 << "-" << (*t).tm_mday << " " << (*t).tm_hour << ":" << (*t).tm_min << ":" << (*t).tm_sec << endl;

	// 该函数返回一个指向字符串的指针，字符串包含了 time 所指向结构中存储的信息，返回形式为：day month date hours:minutes:seconds year\n\0。
	char *buf1 = asctime(t);
	cout << buf1 << endl;

	// 格式化时间
	char buf[26];
	strftime(buf, 26, "%Y-%m-%d %H:%M:%S", t);
	cout << "格式化的时间：" << buf << endl;
}