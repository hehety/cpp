#include <iostream>
#include <cstring>

using namespace std;

double test11(int a, int b);
void test22(char** a);


void cpp_try()
{
	try
	{
		test11(1, 1);
	}
	catch (const char * msg)
	{
		cerr << "error:" << msg << endl;
	}
	catch (const int code)
	{
		cerr << "error:" << code << endl;
	} 
	char* a[] = {"1231111111111111111111111111111111", "221233", "21", "33", "", ""};
	test22(a);
}



double test11(int a, int b)
{
	if (b == 0)
	{
		throw "xxxxx";
	}
	if (a == 0)
	{
		throw 1;
	}
	int c[1] = {1};
	c[100] = 10;
	// 获取数组长度
	cout << (sizeof(c) / sizeof(c[0])) << endl;
	return a / b;
}


void test22(char** a)
{
	cout << "数组长度为：" << (sizeof(*a) / sizeof((*a)[0])) << endl;
	string v[] = {"", "", "" , "" };
	cout << "字符串数组长度为：" << (sizeof(v) / sizeof((v)[0])) << endl;
}