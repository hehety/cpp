#include <iostream>

using namespace std;

#define PI 3.1415926
// 使用 #define 来定义一个带有参数的宏
#define MIN(a, b) (a < b ? a : b)
//# 和 ## 运算符
#define MKSTR(x) #x // cout << MKSTR(HELLO C++); output is "HELLO C++"
#define CONCAT(x, y) x##y // 将x和y拼接作为变量名


#define DEBUG


void cpp_preprocessor()
{
	cout << "PI = " << PI << endl;
	cout << "MIN = " << MIN(1,2) << endl;

	// 条件编译
#ifdef DEBUG
	cout << "this is debug info" << endl;
#endif // DEBUG

	// C++ 中的预定义宏
	cout << "当前行号:" << __LINE__ << endl;
	cout << "当前文件名:" << __FILE__ << endl;
	cout << "编译日期:" << __DATE__ << " " << __TIME__ << endl;



	int xy = 100;
	cout << CONCAT(x, y) << endl;

}