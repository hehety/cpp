#include <iostream>
#include <vector>

using namespace std;

// ����ģ��
template <typename T>
T MaxNum(T a, T b)
{
	T ret = a < b ? b : a;
	cout << "addr:" << &ret << endl;
	return ret;
}

// ��ģ��
template <class T>
class User
{
private:
	vector<T> v;

public:
	void add(T const& i)
	{
		v.push_back(i);
	}
	T const& pop()
	{
		if (v.empty())
		{
			throw "vector is empty";
		}
		T ret = v.back();
		v.pop_back();
		return ret;
	}
};



void cpp_template()
{
	int ret = MaxNum(1, 2);
	cout << "addr:" << &ret << endl;
	cout << "max(1,2) is " << ret << endl;


	User<int> user;

	user.add(123);
	int a = user.pop();
	cout << "ret is " << a << endl;
}