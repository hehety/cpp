#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <string>
#include <../includes/demo.h>

using namespace std;


/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////  CPP base          ///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// strcpy_s和朋友是微软对c的扩展，它们在C11中被标准化。GCC 4.8.5不支持它们

// ‘strcpy’ was not declared in this scope >>>>>>>
// 你需要包含cstring或string.h（它们是编写相同内容的不同方法）。这些不同于字符串; cstring和string.h是c库; just string是一个c ++库。
// 通常，在c ++中，通过添加.h（#include <stdlib.h>）或预先添加ac（#include <cstdlib>）来包含ac头。包含c ++标头时，请始终关闭.h（#include <string>）。
// 无论如何，你混合c和c ++太多了。对字符串使用严格的c和char *或使用c ++并使用std::string。

// Enable multithreading to use std::thread: Operation not permitted >>>
// 在编译命令添加-pthread标志

// This file requires compiler and library support for the ISO C++ 2011 standard. This support is currently experimental, and must be enabled with the -std=c++11 or -std=gnu++11 compiler options. >>>
// 在编译命令添加-std=c++11标志



// 声明变量/函数
int add(int a, int b);
// extern 关键字
extern char* read_book();
extern void cpp_func();
extern void cpp_math();
extern void cpp_array();
extern void cpp_string();
extern void cpp_ptr();
extern void cpp_time();
extern void cpp_io_base();
extern void cpp_struct();
extern void cpp_object();
extern void cpp_io_file();
extern void cpp_try();
extern void cpp_dynamic_mem();
extern void cpp_namespace();
extern void cpp_template();
extern void cpp_preprocessor();
extern void cpp_signal();
extern void cpp_thread();

//在所有函数外部定义的变量（通常是在程序的头部），称为全局变量。全局变量的值在程序的整个生命周期内都是有效的。
//全局变量可以被任何函数访问。也就是说，全局变量一旦声明，在整个程序中都是可用的。下面的实例使用了全局变量和局部变量
int h = 4;
static char* VERSION = "1.0";
static string VERSIONS = "2.0";
const int LIFE = 100;


void call_me()
{
	std::cout << "hello world(" << VERSION << ")" << endl;
	std::cout << VERSIONS << endl;


	//---------------------------------------------------------------
	// 基本类型
	char a = 1;
	char b = 'b';
	unsigned int c = 1;
	float d = 1.021;
	double e = 2.123123123123123123123123123123123123;
	wchar_t f = 78888888888888888;
	cout << sizeof f << endl;
	//---------------------------------------------------------------




	//---------------------------------------------------------------
	auto s = new Sex;
	*s = Female;

	cout << "sex is " << *s << endl;
	cout << "total is  " << add(1, 2) << endl;
	//---------------------------------------------------------------




	//---------------------------------------------------------------
	int g = 85;         // 十进制
	int h = 0213;       // 八进制 
	int i = 0x4b;       // 十六进制 

	cout << i << endl;
	//---------------------------------------------------------------



	//---------------------------------------------------------------
	short unsigned int m;
	short int n;

	m = 50000;
	n = m;

	cout << m << " " << n << endl;
	//---------------------------------------------------------------




	//---------------------------------------------------------------
	auto k("asdasd");
	cout << k << endl;
	//---------------------------------------------------------------


	cout << read_book() << endl;


	//---------------------------------------------------------------
	unsigned int aa = 10;
	unsigned int bb = ~aa;
	float cc = 3.4115;
	cout << aa << " " << bb << " " << (aa >> 1) << endl;
	cout << int(cc) << "   " << &cc << endl;
	//---------------------------------------------------------------


	for (int j = 0; j < 10; j++) {
		cout << j << " ";
	}
	cout << endl;

	unsigned short int dd = 11;
	do
	{
		cout << dd++ << " ";
	} while (dd < 20);
	cout << endl;

	while (dd < 30)
	{
		cout << dd++ << " ";
	}
	cout << endl;

	unsigned short int ee = 3;

	switch (ee)
	{
	case 1:
		cout << "case is " << ee << endl; break;
	case 2:
		cout << "case is " << ee << endl; break;
	case 3:
		cout << "case is " << ee << endl; break;
	default:
		cout << "case is default" << endl; break;

	}

	cout << "case is " << (ee > 3 ? "true" : "false") << endl;
}



int add(int a, int b)
{
	return a + b;
}


int main()
{
	// call_me();
	// cpp_func();
	// cpp_math();
	// cpp_array();
	// cpp_string();
	// cpp_ptr();
	// cpp_time();
	// cpp_io_base();
	// cpp_struct();
	// cpp_object();
	// cpp_io_file();
	// cpp_try();
	//cpp_dynamic_mem();
	//cpp_namespace();
	//cpp_template();
	//cpp_preprocessor();
	//cpp_signal();
	cpp_thread();
	return 0;
}

