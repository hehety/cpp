#include <iostream>
#include <cmath>
#include <ctime>
#include <stdlib.h>

using namespace std;

void cpp_math()
{
	// 数学方法
	int num = 9;
	double a = 1.244;
	double b = 1.567;


	cout << sqrt(num) << endl;
	cout << floor(a) << endl;
	cout << ceil(a) << endl;

	// 随机数
	// 生成随机数之前必须先调用 srand() 函数
	srand((unsigned)time(0));
	for (size_t i = 0; i < 5; i++)
	{
		int r = rand() % 100;
		cout << r << " ";
	}

}