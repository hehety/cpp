#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <cstring>

using namespace std;

void cpp_string()
{
	char a[8] = { 'a','b','c','\0' };
	char b[4] = { 'a','b','c','\0' };

	// 如果 s1 和 s2 是相同的，则返回 0；如果 s1<s2 则返回值小于 0；如果 s1>s2 则返回值大于 0。
	cout << "compare result:" <<strcmp(a, b) << endl; // return 0 

	cout << a << endl;
	cout << b << endl;

	strcat(a, b);
	cout << a << endl;
	cout << strlen(a) << endl;
	char *p = strchr(a, 'c');
	char *pp = NULL;
	cout << (char)*p << endl; // ?????

	cout << "compare result:" << strcmp(a, b) << endl; // return 1

	char c[8];
	strcpy(c, a);
	cout << c << endl;


}